﻿using ADFPlayground.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ADFPlayground
{
    public class HttpStart
    {
        public const string CorrelationIdHeader = "correlation-id";
        private readonly ILogger logger;


        public HttpStart(ILoggerFactory loggerFactory)
        {
            this.logger = loggerFactory.CreateLogger<HttpStart>();
        }

        [FunctionName(nameof(HttpStart))]

        public async Task<ActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "instances")] HttpRequestMessage req,
            [DurableClient] IDurableClient client)
        {
            NameValueCollection queryString = req.RequestUri.ParseQueryString();
            string instanceId = queryString?.GetValues("instanceId")?.FirstOrDefault() ?? Guid.NewGuid().ToString();
            string correlationId = req.Headers.TryGetValues(CorrelationIdHeader, out var correlationHeaderValues) ? correlationHeaderValues.FirstOrDefault() : instanceId;

            OrchestratorInput input = await req.Content?.ReadAsAsync<OrchestratorInput>() ?? new OrchestratorInput();
            input.CorrelationId = correlationId;

            await client.StartNewAsync(nameof(Orchestrator), instanceId, input);


            logger.LogInformation($"Started orchestration with ID = '{instanceId}'.");

            return new OkObjectResult(new
            {
                instanceId,
            });
        }
    }
}
