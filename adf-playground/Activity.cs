﻿using ADFPlayground.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;

namespace ADFPlayground
{
    public class Activity
    {
        private readonly ILogger logger;

        public Activity(ILoggerFactory loggerFactory)
        {
            this.logger = loggerFactory.CreateLogger<Activity>();
        }

        [FunctionName(nameof(Activity))]
        public string Run([ActivityTrigger] IDurableActivityContext context)
        {
            (int index, ActivityInput input) = context.GetInput<(int Index, ActivityInput Input)>();

            this.logger.LogInformation($"Saying {index}-th hello to {input.Name} from {input.Location}.");

            return $"Hello {input.Name} from {input.Location}!";
        }
    }
}
