﻿using System;
using System.Collections.Specialized;
using System.Linq;
using Microsoft.Azure.WebJobs;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;

namespace ADFPlayground
{
    public class HttpTerminate
    {
        internal const string DurableTaskExceptionSourceName = "Microsoft.Azure.WebJobs.Extensions.DurableTask";

        private readonly ILogger logger;

        public HttpTerminate(ILoggerFactory loggerFactory)
        {
            logger = loggerFactory.CreateLogger<HttpTerminate>();
        }

        [FunctionName(nameof(HttpTerminate))]
        public async Task<ActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "instances/{instanceId}")] HttpRequestMessage req,
            [DurableClient] IDurableClient client,
            string instanceId)
        {
            NameValueCollection queryString = req.RequestUri.ParseQueryString();
            string reason = queryString?.GetValues("reason")?.FirstOrDefault();
 
            try
            {
                await client.TerminateAsync(instanceId, reason);

                logger.LogInformation($"Scheduled termination for orchestration with ID = '{instanceId}'.");
            }
            catch (ArgumentException ex) when (ex.ParamName == "instanceId" && ex.Source == DurableTaskExceptionSourceName)
            {
                logger.LogWarning($"Attempted to terminate a non-existent orchestration with ID = '{instanceId}'.");
            }
            catch (InvalidOperationException ex) when (ex.Source == DurableTaskExceptionSourceName)
            {
                logger.LogWarning($"Attempted to terminate a non-running orchestration with ID = '{instanceId}'.");
            }

            return new OkResult();
        }
    }
}
