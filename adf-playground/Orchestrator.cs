using ADFPlayground.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ADFPlayground
{
    public class Orchestrator
    {
        private readonly ILogger logger;

        public Orchestrator(ILoggerFactory loggerFactory)
        {
            this.logger = loggerFactory.CreateLogger<Orchestrator>();
        }

        [FunctionName(nameof(Orchestrator))]
        public async Task<List<string>> RunAsync(
            [OrchestrationTrigger] IDurableOrchestrationContext context)
        {
            var input = context.GetInput<OrchestratorInput>();

            var outputs = new List<string>();

            for (int i = 0; i < input.ActivityInputs.Length; i++)
            {
                ActivityInput activityInput = new ActivityInput
                {
                    Location = input.ActivityInputs[i].Location,
                    Name = input.ActivityInputs[i].Name,
                    CorrelationId = input.CorrelationId,
                };
                outputs.Add(await context.CallActivityAsync<string>(nameof(Activity), (i, activityInput)));
            }

            return outputs;
        }
    }
}