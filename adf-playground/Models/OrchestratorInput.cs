﻿using System;

namespace ADFPlayground.Models
{
    public class OrchestratorInput : ICorrelatableInput
    {
        public ActivityInput[] ActivityInputs { get; set; } = Array.Empty<ActivityInput>();

        public string CorrelationId { get; set; }
    }
}
