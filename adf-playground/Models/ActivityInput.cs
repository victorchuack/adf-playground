﻿namespace ADFPlayground.Models
{
    public class ActivityInput : ICorrelatableInput
    {
        public string Name { get; set; }

        public string Location { get; set; }

        public string CorrelationId { get; set; }
    }
}
