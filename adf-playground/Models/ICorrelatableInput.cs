﻿namespace ADFPlayground.Models
{
    public interface ICorrelatableInput
    {
        string CorrelationId { get; }
    }
}
